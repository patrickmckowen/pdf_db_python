'''

Functions related to handling problems related to differences in versions.
For instance the subprocess.call(), which is needed for pdf text extraction,
is not available in versions before 5.15 (>=5.1.5)

'''
import sys

def version(v_string):
    '''
    The version function is needed because, while developing this tool,
    I found many of the functions and methods from the built in library
    used in the code presented problems when using the code on another machine.
    Since many (of at least my) computers are not powerful enough to use
    docker all the time, I'm choosing to include this little extra to manually
    cater to the needs of backward compatability.

    Give this function a string of the version number requirement.
    Examples:
    - "3.14"
    - "<3.14"
    - ">3.14"
    - "<=3.14"
    - etc.

    It will return true or false.
    '''
    # Pull the comparison operator if it's provided
    # comparison_operator = re.
    
