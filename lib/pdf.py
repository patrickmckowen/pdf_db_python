import subprocess
import os
def text_from_file(*file_paths):
    for file_path in file_paths:
        args = ["pdftotext",
                '-enc',
                'UTF-8',
                file_path,
                '-']
        res = subprocess.run(args, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        output = res.stdout.decode('utf-8')
        return output

if __name__ == '__main__':
	print(text_from_file(os.path.join(os.getcwd(), "natural_language_processing_in_python3.pdf")))
